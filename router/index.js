import express from "express";
export const router = express.Router();
export default { router };

router.get("/", (req, res) => {
  res.render("index", { titulo: "PreExamen02 [Ruanet Ozuna]" });
});

router.get("/pago", (req, res) => {
  const params = {
    titulo: "PreExamen02 [Ruanet Ozuna]",
    numero: req.query.numero,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    servicio: req.query.servicio,
    kwConsumidos: req.query.kwConsumidos,
    isPost: false,
  };
  res.render("pago", params);
});

router.post("/pago", (req, res) => {
  const precios = [1.08, 2.5, 3.0];
  const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
  const precioKw = precios[parseInt(servicio)]; // Convertir servicio a un número entero
  const tipoDeServicio = servicio == 0 ? 'Domestico' : servicio == 1 ? 'Comercial' : 'Industrial';
  const subtotal = precioKw * kwConsumidos;
  
  let descuento = 0;
  if (kwConsumidos <= 1000) {
    descuento = 0.1;
  } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
    descuento = 0.2;
  } else {
    descuento = 0.5;
  }

  const descuentoAplicado = subtotal * descuento;
  const subtotalConDescuento = subtotal - descuentoAplicado;
  const impuesto = 0.16 * subtotal;
  const total = (subtotal + impuesto) - descuentoAplicado;
  
  const params = {
    titulo: "PreExamen02 [Ruanet Ozuna]",
    numero,
    nombre,
    domicilio,
    servicio: tipoDeServicio,
    kwConsumidos,
    precioKw,
    subtotal: subtotal.toFixed(2), // Formatear a 2 decimales
    descuento: descuentoAplicado.toFixed(2), // Formatear a 2 decimales
    subtotalConDescuento: subtotalConDescuento.toFixed(2), // Formatear a 2 decimales
    impuesto: impuesto.toFixed(2), // Formatear a 2 decimales
    total: total.toFixed(2), // Formatear a 2 decimales
    isPost: true,
  };

  res.render("pago", params);
});
